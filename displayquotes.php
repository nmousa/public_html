<?php
session_start();
$name = $_SESSION["lemail"];

   if (isset($_SESSION["access_granted"]) && !$_SESSION["access_granted"] || !isset($_SESSION["access_granted"])) {
      $_SESSION["status"] = "* Please login to view quotes";
      header("Location:quote.php");
   }

  require_once "php/Dao.php";
  $dao = new Dao();
  ?>
  
  <html>
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Muslim Student Association - Boise State University</title>
      <link href="css/style.css" rel="stylesheet" type="text/css" />
  </head>
  
  <?php include("php/title.php");?>
  
  <body>
      <div id="menuContainer">
        <?php include_once("php/menu.php");?>
      </div>
      <div id="bodyContainer">
      	<div id="bodyContentContainer">
          	<h1>Quotes</h1><hr>
          	    <?php
                  $quotes = $dao->getQuotes();
                  echo "<table>";
                  foreach ($quotes as $quote) {
                    echo "<tr>";
                    echo "<td>" . $quote["quote"] . "</td>";
                    echo "<td>" . $quote["source"] . "</td>";
                    echo "</tr>";
                  }
                  echo "</table>";
                  ?>
      	</div>
      </div>
  </body>
  <?php include("php/footer.php");?>
</html>
