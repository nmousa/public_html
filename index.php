<html>
  <head>
  <link href="css/style.css" rel="stylesheet" type="text/css" />
  <title>Muslim Student Association - Boise State University</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
     	<script language="Javascript" type="text/javascript" src="js/jquery-1.4.1.js"></script>
   	  <script language="Javascript" type="text/javascript" src="js/jquery.blinds-0.9.js"></script>
  </head>
  
  <body>
  
  <?php include("php/title.php");?>
  
  
  <div id="menuContainer">
  <?php include_once("php/menu.php"); ?>
  </div>
  
  <div id="bodyContainer">
  	<div id="bodyContentContainer">
  	
  	      	<h1> Welcome to MSA Offcial Web Page!</h1>
  	    <div id="clear">
  	    <div id="lembed">
      	<p> The Muslim Student Association chapter at Boise State University exists to  
      	    support the spiritual, social and personal needs of Muslim Students and 
      	    anyone interested in Islam at Boise State University. It is run by 
      	    students for students, in order to serve and unite the BSU community.
      	
      	</p>
      	
      	</div>
      
        <div id="embed">	
        	<div class="slideshow">
           <ul>
              <li><img src="img/msa_logo.jpg" alt="logo" /></li>
              <li><img src="img/new_msa_logo.PNG" alt="logo" /></li>
              <li><img src="img/msa_logo.jpg" alt="logo" /></li>
              <li><img src="img/new_msa_logo.PNG" alt="logo"/></li>
              <li><img src="img/msa_logo.jpg" alt="logo" /></li>
              <li><img src="img/new_msa_logo.PNG" alt="logo" /></li>
              <li><img src="img/msa_logo.jpg" alt="logo" /></li>
           </ul>
        </div>
        
        <a href="#" class="change_link" onclick="$('.slideshow').blinds_change(0)">1</a>
        <a href="#" class="change_link" onclick="$('.slideshow').blinds_change(1)">2</a>
        <a href="#" class="change_link" onclick="$('.slideshow').blinds_change(2)">3</a>
        <a href="#" class="change_link" onclick="$('.slideshow').blinds_change(3)">4</a>
        <a href="#" class="change_link" onclick="$('.slideshow').blinds_change(4)">5</a>
        <a href="#" class="change_link" onclick="$('.slideshow').blinds_change(5)">6</a>
        <a href="#" class="change_link" onclick="$('.slideshow').blinds_change(6)">7</a>
        
        
        		<script type="text/javascript">
        			$(window).load(function () {
        				$('.slideshow').blinds();
        			})
        		</script>
        		
        		</div>
        		</div>
  </div>
  </div>
  </body>
  <?php include("php/footer.php");?>
</html>
