<?php 
 
 session_start();
 $session_token= md5(uniqid());
 $_SESSION["session_token"] = $session_token;


if (!(isset($_SESSION["access_granted"]) && !$_SESSION["access_granted"] || !isset($_SESSION["access_granted"]))) {
     //alredy logged in 
     header("Location:mypage.php");
   }

/* varibales for forms */
$d_email = $d_lemail= $status= $d_phone_num = $poem_checked = $quote_checked = "";
$s_verizon = $s_att = $s_sprint = $s_tmobile = "";
$s_cellular =  $s_leap = $s_idk = "";
$phone_numErr = $emailErr = $passwordErr = "";

/***************************************************/
/**-------emails and errors ----------------/
/***************************************************/
if(!empty($_SESSION["email"]))
$d_email = $_SESSION["email"];
if(!empty($_SESSION["lemail"]))
$d_lemail = $_SESSION["lemail"];
if(!empty($_SESSION["login_emailErr"]))
$login_emailErr = $_SESSION["login_emailErr"];
if(!empty($_SESSION["status"]))
$status = $_SESSION["status"];
if(!empty($_SESSION["emailErr"]))
$emailErr = $_SESSION["emailErr"];
if(!empty($_SESSION["phone_numErr"]))
$phone_numErr = $_SESSION["phone_numErr"];
if(!empty($_SESSION["passwordErr"]))
$passwordErr = $_SESSION["passwordErr"];

/***************************************************/
/**-------get last input for phone number-----------/
/***************************************************/
if(!empty($_SESSION["phone_num"]))
$d_phone_num = $_SESSION["phone_num"];

/***************************************************/
/**-------get last input for provider --------------/
/***************************************************/
if(!empty($_SESSION["verizon"])){
$s_verizon = "selected";
}  if(!empty($_SESSION["att"])){
$s_att = "selected=\"selected\"";
}  if(!empty($_SESSION["sprint"])){
$s_sprint = "selected";
}  if(!empty($_SESSION["tmobile"])){
$s_tmobile = "selected";
}  if(!empty($_SESSION["cellular"])){
$s_cellular = "selected";
}  if(!empty($_SESSION["leap"])){
$s_leap = "selected";
}  if(!empty($_SESSION["idk"])){
$s_idk = "selected";
}

/***************************************************/
/**-------get last input for text type -------------/
/***************************************************/
if (!empty($_SESSION["want_poem"])){
$poem_checked = "checked"; 
}
if (!empty($_SESSION["want_quote"])){
$quote_checked = "checked"; 
}
?> 

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Muslim Student Association - Boise State University</title>
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
        <script type="text/javascript" src="jq/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="jq/valid.js"></script>
    </head>

    <?php include("php/title.php");?>

    <body>
        <div id="menuContainer">
            <?php include_once("php/menu.php");?>
        </div>
        <div id="bodyContainer">
            <div id="bodyContentContainer">
              <div id="clear">
              <h1>Welcome to Daily Quote </h1>
                <p id="lembed" > Sign up for <strong>once-in-lifetime opportunity</strong> to receive daily quotes or poems from Rumi. 
                    Note that if you sign up for Rumi Poem and Rumi Quote we will only send you only one 
                    text randomly from a combined category poems and quotes. <br><br><br>
                    In your light I learn how to love. In your beauty, how to make poems. 
                    You dance inside my chest where no-one sees you, but sometimes I do, 
                    and that sight becomes this art. <br>  -Rumi
		    <br><br>
		    


		</p>
                <div>
                <div id="embed">
                    <form action="php/validatelogin.php" method="POST">
                        <h1> Login </h1>
                        <div>
                            <label>Email</label><input type="text" value="<?php echo $d_lemail; ?>" name="lemail" required>
                            <span></span><br>
                        </div>
                        <label>Password</label><input type="password" name="lpassword" required><br>
                         <input type="hidden" name="session_token" value="<?php echo $session_token; ?>" />
                        <br><input type="submit" value="Log In"><br>

                        <?php 
                        if(!empty($login_emailErr)){
                        echo " <table > <tr> <td> $login_emailErr  $status </td>  </tr> </table>"; 
                        }?>                
                    </form>
                    <form action="php/validatesignup.php" method="POST">
                        <h2>Sign Up for Daily Text</h2>
                        <div>
                        <label>Email</label>           <input type="text" name="email"  value="<?php echo $d_email; ?>" required>
                        <span></span><br>
                        </div>

                        <label>Password</label>       <input type="password" name="password" required><br>
                        <label>Re-Enter Password</label><input type="password" name="repassword" required><br>
                        <label>Phone #</label>   <input type="text" name="phone_num" value="<?php echo $d_phone_num; ?>" required>
                        <span id="phone_numErr" ></span> <br>
                        Phone Provider     <select type="text" id="phone_provider" name="phone_provider" required><br>
                            <option <?php echo $s_verizon; ?> value="verizon">Verizon Wireless </option>
                            <option <?php echo $s_att; ?> value="att">AT &amp; T Mobility</option>
                            <option <?php echo $s_sprint; ?> value="sprint">Sprint Corporation</option>
                            <option <?php echo $s_tmobile; ?> value="tmobile">T-Mobile US</option>
                            <option <?php echo $s_cellular; ?> value="cellular">U.S. Cellular</option>
                            <option <?php echo $s_leap; ?> value="leap">Leap Wireless</option>
                            <option <?php echo $s_idk; ?> value="idk">No idea...</option>
                        </select><br>
                        <input type="checkbox" name="want_poem" value="want_poem" <?php echo $poem_checked; ?> > Rumi Poem <br>
                        <input type="checkbox" name="want_quote" value="want_quote" <?php echo $quote_checked; ?> >Rumi Quote <br> 
                        <input type="hidden" name="session_token" value="<?php echo $session_token; ?>" />
                        <br><input type="submit" value="Submit"><br><br>
			<div id="error"> 
		        <?php echo  $status; 
                        if(!empty($emailErr) || !empty($passwordErr) || !empty($phone_numErr)){
                        echo " <table> <tr> <td> $emailErr $passwordErr $phone_numErr </td>  </tr> </table>";
                        }
                        ?> 
			</div>

                    </form>
                </div></div>
            </div></div>
        </div>
    </body>
    <?php include("php/footer.php");?>
</html>
