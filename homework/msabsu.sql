/*
* @author Nilab Mohammad Mousa 
* Homework 5 - Web Dev 
* Tables for msabsu.org 
*/

/* information needed from the user */
CREATE TABLE user (
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	email VARCHAR(256) NOT NULL,
	password VARCHAR(64) NOT NULL,
	phone_num INTEGER(10) NOT NULL,
	phone_provider VARCHAR(256), 
	want_poem TINYINT(1),
	want_quote TINYINT(1)
);


/* table to store poems */ 
CREATE TABLE poem (
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	poem VARCHAR(1024) NOT NULL,
	source VARCHAR(256) NOT NULL
); 

/* table to store quotes */
CREATE TABLE quote (
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	quote VARCHAR(1024) NOT NULL,
	source  VARCHAR(256) NOT NULL
); 

