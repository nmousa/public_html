INSERT INTO poem (poem, source) 
VALUES
("What you seek is seeking you" , "Rumi"),
("Everything in the universe is within you. Ask all from yourself.", "Rumi");

INSERT INTO quote (quote, source) 
VALUES
("In your light I learn how to love. In your beauty, how to make poems. You dance inside my chest where no-one sees you, but sometimes I do, and that sight becomes this art." , "Rumi"),
("Lovers don't finally meet somewhere. They're in each other all along.", "Rumi");
