<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Muslim Student Association - Boise State University</title>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
    </head>

    <?php include("php/title.php");?>

    <body>
        <div id="menuContainer">
            <?php include_once("php/menu.php");?>
        </div>
        <div id="bodyContainer">
            <div id="bodyContentContainer">
                <h1>Events</h1>
                <div id="embed"> <strong> Islamic Awareness Week </strong> will be held during Spring semester! </div>
                <?php include_once("php/calendar.php"); ?> 
            </div>
        </div>
    </body>
    <?php include("php/footer.php");?>
</html>
