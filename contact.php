<html>

  <head> 
      <title>Muslim Student Association - Boise State University</title>
      <link href="css/style.css" rel="stylesheet" type="text/css" />
  </head>

  <?php include("php/title.php");?>

  <body>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>


      <div id="menuContainer">
        <?php include("php/menu.php");?>
      </div>
        <div id="bodyContainer">
        	<div id="bodyContentContainer">
        	<h1> MSA@BSU Contact Info </h1>
        	  <h3 > Organization Advisor</h3>
        	      <p> Monk Wells 
        	      <br> stevenwells@boisestate.edu 
        	      <br> 208-000-0001
        	      </p>
            <h3 > Contact Person</h3>
                <p> Nilab Mohammad Mousa 
                <br> nilabmohammadmousa@u.boisestate.edu
                <br> 208-000-0000
                </p>
              <p class="fb-like" data-href="https://www.facebook.com/pages/MSA-BSU/161727664891" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></p>
        	</div>
        </div>
  </body>
  <?php include("php/footer.php");?>
</html>