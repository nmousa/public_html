<?php

class Dao{

  private $host = "localhost";
  private $user = "nmousa";
  private $db = "nmousa";
  private $pass = 'webdev@nil';
  
  public function __construct () {

  }

 public function getConnection () {
    $newconn ="";
    try{
      $newconn = new PDO("mysql:host={$this->host};dbname={$this->db}", $this->user, $this->pass);
    }catch(PDOException $e){
      print "Error!: " . $e->getMessage();
    }
    return $newconn;
  }


  public function addNewUser ($email, $password, $phone_num, $phone_provider, $want_poem, $want_quote){
    
    $password = hash('sha256', $password . "CorrectHoursBatteryStaple");
    $conn = $this->getConnection();
    $saveQuery =
        "INSERT INTO user
        (email, password, phone_num, phone_provider, want_poem, want_quote)
        VALUES
        (:email, :password, :phone_num,:phone_provider, :want_poem, :want_quote)";
    $q = $conn->prepare($saveQuery);
    $q->bindParam(":email", $email);
    $q->bindParam(":password", $password);
    $q->bindParam(":phone_num", $phone_num);
    $q->bindParam(":phone_provider", $phone_provider);
    $q->bindParam(":want_poem", $want_poem);
    $q->bindParam(":want_quote", $want_quote);
    $q->execute();
    
  }
  
  
  public function isUser($email, $password){
    $rval = $this->getPassword($email);
    $enPassword = hash('sha256', $password . "CorrectHoursBatteryStaple");
    if($rval == $enPassword){
      return true;
    }else{
      return false;
    }
  }
  
  public function getPassword($email ){
    $conn = $this->getConnection();
    $getQuery = "SELECT password FROM user WHERE email = :email";
    $q = $conn->prepare($getQuery);
    $q->bindParam(":email", $email);
    $q->execute();
    return reset(reset($q->fetchAll()));
  }
  
  
  public function getPoems () {
    $conn = $this->getConnection();
    return $conn->query("SELECT * FROM poem");
  }
  
  public function getQuotes () {
    $conn = $this->getConnection();
    return $conn->query("SELECT * FROM quote");
  }
  
  public function getPhoneNums () {
    $conn = $this->getConnection();
    $getQuery = "SELECT phone_num FROM user";
    $q = $conn->prepare($getQuery);
    $q->execute();
    return reset($q->fetchAll());
    
  }
    
  public function getPoem(){
    $conn = $this->getConnection();
    $getQuery = "SELECT COUNT(*) FROM poem";
    $q = $conn->prepare($getQuery);
    $q->execute();
    $num = reset(reset($q->fetchAll()));

    $id= rand(1, num);

    $getQuery = "SELECT poem FROM poem WHERE id = $id";
    $q = $conn->prepare($getQuery);
    $q->bindParam(":id", $id);
    $q->execute();
    return reset(reset($q->fetchAll()));
  }
  
  public function getQuote(){
    $conn = $this->getConnection();
    $getQuery = "SELECT COUNT(*) FROM quote";
    $q = $conn->prepare($getQuery);
    $q->execute();
    $num = reset(reset($q->fetchAll()));

    $id= rand(1, $num);
    $getQuery = "SELECT quote FROM quote WHERE id = id";
    $q = $conn->prepare($getQuery);
    $q->bindParam(":id", $id);
    $q->execute();
    $rval = reset(reset($q->fetchAll()));
    return $rval;
  }
    
  public function getRandomText($phone_num){
    $quote_want=0;
    $poem_want=0;
    $rval = "";
    
    $conn = $this->getConnection();
    $getQuery = "SELECT want_poem FROM user WHERE phone_num = phone_num";
    $q = $conn->prepare($getQuery);
    $q = $conn->prepare($getQuery);
    $q->bindParam(":phone_num", $phone_num);
    $q->execute();
    $poem_want = reset(reset($q->fetchAll()));
    
    $getQuery = "SELECT want_quote FROM user WHERE phone_num = " . $phone_num;
    $q = $conn->prepare($getQuery);
    $q->bindParam(":phone_num", $phone_num);
    $q->execute();
    $quote_want = reset(reset($q->fetchAll()));
    
    if(($quote_want==1) and ($poem_want==1)){
        $val = rand(1,2);
        if($val==1){
           $rval = $this->getPoem();
        }else{
           $rval = $this->getQuote();
        }
    }elseif ($quote_want==1){
           $rval = $this->getPoem();
    }elseif ($poem_want==1) {
           $rval = $this->getQuote();
    }
    return $rval;
    }
  
  
  public function getProvider($phone_num){
    $conn = $this->getConnection();
    $getQuery = "SELECT phone_provider FROM user WHERE phone_num = phone_num";
    $q = $conn->prepare($getQuery);
    $q->bindParam(":phone_num", $phone_num);
    $q->execute();
    $provider = reset(reset($q->fetchAll()));

    $rval = "";
    switch ($provider) {
    case "verizon":
        $rval= "vtext.com";
        break;
    case "att":
        $rval= "txt.att.net";
        break;
    case "sprint":
        $rval= "messaging.sprintpcs.com";
        break;
    case "tmobile":
        $rval= "tmomail.net";
        break;
    case "cellular":
        $rval= "email.uscc.net";
        break;
    case "leap":
        $rval= "mmode.com";
        break;
    }
    return $rval;
  }
}
