<?php

    session_start();
    $session_token = $_SESSION["session_token"];
    unset($_SESSION["session_token"]);
    if($session_token && $session_token != $_POST["session_token"]){
          $_SESSION["status"] = " * Just create an account and login, no reason to attack this poor website";
          header("location:../quote.php");
          die();          
    }
    
    require_once "Dao.php";
    $dao = new Dao();
    
    $emailErr = $passwordErr = $repasswordErr = $phone_numErr = $providerErr = $serviceErr =  "";
    $email = $password = $repassword = $phone_num = $phone_provider = $want_poem = $want_quote ="";
    $phone_pattern = "^((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}$";
 
 
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  
    /* email needs to be validated as well*/
    if (empty($_POST["email"])) {
        $_SESSION["emailErr"] = "Email field cannot be empty";
    } else {
        $_SESSION["email"] = $_POST["email"];
    }
 
    /*password and need to encrypt*/
    if (empty($_POST["password"]) ||empty($_POST["repassword"]) ) {
        $_SESSION["passwordErr"] = "Password fields cannot be empty";
    } else if($_POST["password"] != $_POST["repassword"]){
        $_SESSION["passwordErr"] = "The passwords you entered do not match";
    } else {
        $passwrd = $_POST["password"];
    }
 
    /*phone number validate !!!!!  */
    if (empty($_POST["phone_num"]))  {
        $emailErr = "Phone number field cannot be empty";
    }
    else {
        $_SESSION["phone_num"] = $_POST["phone_num"];
    }
    
    if (preg_match($email_pattern, $_POST["phone_num"])){
        $_SESSION["phone_numErr"] = "Input is not a valid phone number";
        $_SESSION["phone_num"] = $_POST["phone_num"];
    }else{
        $_SESSION["phone_num"] = $_POST["phone_num"];
        $_SESSION["phone_numErr"] = "";
      }
        
      /******************************************************************/
    /*phone provider */
    if (empty($_POST["phone_provider"]))  {
        $providerErr = "Phone provider field cannot be empty";
    } else {
        $phone_provider = $_POST["phone_provider"];
    }  if ($_POST["phone_provider"] == "verizon")  {
         $_SESSION["verizon"] = "true";
    }else{
         $_SESSION["verizon"] = "";
    }  if ($_POST["phone_provider"] == "att"){
         $_SESSION["att"] = "true";
    }else{
         $_SESSION["att"] = "";
    }  if ($_POST["phone_provider"] == "sprint"){
         $_SESSION["sprint"] = "true";
    }else{
         $_SESSION["sprint"] = "";
    }  if ($_POST["phone_provider"] == "tmobile"){
         $_SESSION["tmobile"] = "true";
    }else{
         $_SESSION["tmobile"] = "";
    }  if ($_POST["phone_provider"] == "cellular"){
         $_SESSION["cellular"] = "true";
    }else{
         $_SESSION["cellular"] = "";
    } if ($_POST["phone_provider"] == "leap"){
         $_SESSION["leap"] = "true";
    }else{
         $_SESSION["leap"] = "";
    } if ($_POST["phone_provider"] == "idk"){
         $_SESSION["idk"] = "true";
    }else{
         $_SESSION["idk"] = "";
    }
    /******************************************************************/
    /*sign up for at least one */
    if (!isset($_POST["want_poem"]) && !isset($_POST["want_poem"])) {
        $serviceErr = "You must select at least one option";
    }
    
    if (isset($_POST["want_poem"])){
         $_SESSION["want_poem"] = "true";
    }else{
        $_SESSION["want_poem"] = "";
    }
    
    if (isset($_POST["want_quote"])){
         $_SESSION["want_quote"] = "true";
    }else{
         $_SESSION["want_quote"] = "";
    }
    
    
      $email = (isset($_POST["email"])) ? $_POST["email"] : "";
      $password = (isset($_POST["password"])) ? $_POST["password"] : "";
      $phone_num = (isset($_POST["phone_num"])) ? $_POST["phone_num"] : "";
      $phone_provider = (isset($_POST["phone_provider"])) ? $_POST["phone_provider"] : "";
      $want_poem = (isset($_POST["want_poem"])) ? "1": "0";
      $want_quote = (isset($_POST["want_quote"])) ? "1" :"0";
      
      $dao->addNewUser($email, $password, $phone_num, $phone_provider, $want_poem, $want_quote);
      
      $_SESSION["access_granted"] = true;

      header("location:../mypage.php");
      die();
    
    /******************************************************************/
      header("Location:../quote.php");
}
