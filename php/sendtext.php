<?php
    require_once "Dao.php";
    $dao = new Dao();
    $phone_nums = $dao->getPhoneNums();

    
    foreach ($phone_nums as $phone_num) {
      $carrier = $dao->getProvider($phone_num);
      $text = $dao->getRandomText($phone_num);
      $message = wordwrap( $text, 171 );  
      $to = $phone_num . '@' . $carrier;  
      $result = mail( $to, '', $message );        
     }
     header("location:../index.php");
 
