<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Muslim Student Association - Boise State University</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
  </head>
  
  <?php include("php/title.php");?>
  
  <body>
    <div id="menuContainer">
      <?php include_once("php/menu.php");?>
    </div>
    <div id="bodyContainer">
    	<div id="bodyContentContainer">
  <h1 >Islamic Center of Boise</h1>
  <div id="clear">
  <div id="lembed" >
        	<p> The Islamic Center of Boise (ICB) is a place of worship 
        	and a place to hold events for the Muslims of Boise and the 
        	surrounding area; all others are welcome to come and join us 
        	or sit back and observe our community. We have a vast community 
        	of Muslims from all over the world that come to the Islamic Center 
        	of Boise for worship and other activities. The ICB holds five daily 
        	prayers seven days a week, the Friday congregational prayer (Al-Jummah), 
        	Taraweeh prayers during the month of Ramadhan, and the two Eid Holiday 
        	prayers. ICB also conducts various educational activities for adults 
        	and children. </p>
        	
        	 <p> <img src="img/bic.jpg"> </p>
  </div>
  <div id=embed> <?php include_once("php/map.php"); ?> </div>
  </div>

    	</div>
    </div>
  </body>
  <?php include("php/footer.php");?>
</html>