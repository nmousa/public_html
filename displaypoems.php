<?php
session_start();
$name = $_SESSION["lemail"];

   if (isset($_SESSION["access_granted"]) && !$_SESSION["access_granted"] || !isset($_SESSION["access_granted"])) {
   $_SESSION["status"] = "* Please login to view poems";
   header("Location:quote.php");
   }

  require_once "php/Dao.php";
  $dao = new Dao();
  ?>
  
  <html>
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Muslim Student Association - Boise State University</title>
      <link href="css/style.css" rel="stylesheet" type="text/css" />
  </head>
  
  <?php include("php/title.php");?>
  
  <body>
      <div id="menuContainer">
        <?php include_once("php/menu.php");?>
      </div>
      <div id="bodyContainer">
      	<div id="bodyContentContainer">
          	<h1>Poems</h1><hr>
          	    <?php
                  $poems = $dao->getPoems();
                  echo "<table>";
                  foreach ($poems as $poem) {
                    echo "<tr>";
                    echo "<td>" . $poem["poem"] . "</td>";
                    echo "<td>" . $poem["source"] . "</td>";
                    echo "</tr>";
                  }
                  echo "</table>";
                  ?>
      	</div>
      </div>
  </body>
  <?php include("php/footer.php");?>
</html>
