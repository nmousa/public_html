/**
 *
 *
 *@author Nilab Mohammad Mousa
 * */
$(document).ready(function() {
    var form = $("#form1");
    var $emails = $("[name='lemail'], [name='email']"); 
    var $phone_num = $("[name='phone_num']");
    var $phone_numErr = $("#phone_numErr");
 

    form.submit(function() {
        if (validateLemail()) {
            return true;
        } else {
            return false;
        }
    });

    function validateEmail($email, $emailInfo) {
        //validation for empty
        if ($email.val() == "") {
            $email.addClass("error");
            $emailInfo.text("Please enter email address");
            $emailInfo.addClass("error");
            return false;
        } else {
            $email.removeClass("error");
            $emailInfo.text("*");
            $emailInfo.removeClass("error");
        }
      
        
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        var filter = /^((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}$/;
        if (pattern.test($email)) {
            $email.removeClass("error");
            $emailInfo.text("*");
            $emailInfo.removeClass("error");
            return true;
        }
        else {
            $email.addClass("error");
            $emailInfo.text("");
            $emailInfo.addClass("error");
            return false;
        }
    }
    
    function validatePhone() {
        var pfilter = /^[1-9]\d{9}$/;
        if (pfilter.test($phone_num.val())) {
            $phone_num.removeClass("error");
            $phone_numErr.text("");
            $phone_numErr.removeClass("error");
            return true;
        }
        else {
            $phone_num.addClass("error");
            $phone_numErr.text("Incorrect phone format! Enter 10 digits");
            $phone_numErr.addClass("error");
            return false;
        }
        
    }

    $emails.off('change').on('change', function() {
        validateEmail($(this), $(this).siblings('span'));
        
    });
    
    $phone_num.off('change').on('change', function() {
        validatePhone();

    });
    
    
});


